#  Cloud9 Desktop App (Linux-x64) #

This repository **holds a packaged electron app** created with nativefier

Nativefier is a command line tool that allows you to easily create a desktop application for any web site with succinct and minimal configuration. Apps are wrapped by Electron in an OS executable (.app, .exe, etc.) for use on Windows, OSX and Linux. Visit https://github.com/jiahaog/nativefier for further information.



To create the app I simply ran these two cmds:


```
#!bash
sudo npm install nativefier -g
nativefier --name "C9" "https://c9.io"
```

![Screenshot_20160508_014719.png](https://bitbucket.org/repo/8kdGgg/images/345290192-Screenshot_20160508_014719.png)